#!/usr/bin/env python

import csv
import urllib

"""
Get engine shed data!

wget -c http://alexdutton.co.uk/engine-shed.csv

full timestamp, UTC timestamp, light (0-255), PIR/movement (0 or 1), relative humidity (%), temperature (C), low battery, not used
"""

def printrow (d):
	print "Timestamp: %s UTC\nLight level [0-255]: %s\nRelative humidity: %s%% \nTemperature: %sC" % (d[1], d[2], d[4], d[5])

# Resource names
url = "http://alexdutton.co.uk/engine-shed.csv"
filename = url.split('/')[-1]

# Get the data and write to a file
onlinedata = urllib.urlopen(url)
localdata = onlinedata.read()
localdata = localdata.split("\n")
onlinedata.close()

localdata = csv.reader( localdata )
data = []
for i in localdata:
	data.append( list(i) )

print "Last recorded data in the engine room:"
printrow (data[-2])
